'use strict';

// from https://help.oclc.org/Metadata_Services/CONTENTdm/Advanced_website_customization/JavaScript_and_CSS_examples/Load_multiple_JavaScript_files
// helper function to load js file and insert into DOM
// @param {string} src link to a js file
// @returns Promise

function loadScript(src) {
return new Promise(function (resolve, reject) {
var script = document.createElement('script');
script.crossOrigin = 'anonymous';
script.src = src;
script.onload = resolve;
        script.onerror = reject;
        document.head.appendChild(script);
    });
}

// helper function to assemble full URL of each JS file
function wholeUrl(file) {
return baseUrl + filePath + file;
}

const baseUrl = window.location.origin ? window.location.origin + '/' : window.location.protocol + '//' + window.location.host + '/';

// path corresponding to directory where JS scripts are uploaded
const filePath = 'customizations/global/pages/js/';

// array containing file names of each JS file
const scriptFilesToLoad =
['video-embed.js', 'right-click-links.js', 'mirador-custom-page.js', 'field-insert-iiif.js', 'canonical-redirect-1_3.js', 'bookreader-cp-1_2.js'];

(function () {
const allScripts = scriptFilesToLoad.map(wholeUrl);
allScripts.forEach(loadScript);
})();