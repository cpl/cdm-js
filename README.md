# cdm-js

the scripts that customize our contentDM instance through contentdm's website configuration tool that's written in react/js. 

LICENSE:
Most of the code substantially consists of code written by OCLC employees 
and the license for this code is TBD. 

Where the code written by me; it is MIT license. 

To use: 
(url-of-your-cdm-instance-hosted-by-oclc)/config/configuretoolresponsive 

Usage: 

See: 
https://help.oclc.org/Metadata_Services/CONTENTdm/Advanced_website_customization/Custom_pages/Working_with_the_file_manager
https://help.oclc.org/Metadata_Services/CONTENTdm/Advanced_website_customization

NOTE: ONLY the loader.js (which I believe has to be named as such) - is uploaded in the custom scripts folder)

the other JS files are to be uploaded through 'custom pages' into a folder named 'js'
(website configuration tool > custom > custom pages)
(requires access contentdm - that only CDPL staff (Amia and Rachel) have. )

YOU MUST HIT PUBLISH BUTTON every time after you upload a file on contentdm's website configuration tool

for loader.js and the custom scripts folder;
contentdm's website will not say whether the upload to the custom scripts folder was successful once you upload it through the GUI interface
(not even a file manager)

(why did I structure files like this:
contentdm lets you only reference ONE js file; so you have to then call each file in an array, as I did in
loader.js)
