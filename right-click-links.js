// button/link that enables right-clicking
// https://help.oclc.org/Metadata_Services/CONTENTdm/Advanced_website_customization/JavaScript_and_CSS_examples/Search_results_open_new_window
(function() {
'use strict';

  const addExternal = function() {
    const searchResults = document.querySelectorAll('div.SearchResult-container');
    const searchResultsTitle = document.querySelectorAll('h2.MetadataFields-header');

    for (let i = 0; i < searchResults.length; i++) {
      const thumbImg = searchResults[i].querySelector('div.SearchResult-thumbnailHolder>img');
      const srcUrl = new URL(thumbImg.src);
      const pathParts = srcUrl.pathname.split("/");
      const collAlias = pathParts[5];
      const itemId = pathParts[7];
      const baseUrl = window.location.origin
        ? window.location.origin + '/'
        : window.location.protocol + '//' + window.location.host + '/';
      const externalTargetUrl = baseUrl + 'digital/collection/' + collAlias + '/id/' + itemId;

      searchResultsTitle[i].insertAdjacentHTML('beforeend', "<a target='_blank' style='float: right;' href='#'><span class='fa fa-external-link'></span></a>");
      searchResultsTitle[i].querySelector('a').href = externalTargetUrl;
      searchResultsTitle[i].querySelector('a').addEventListener('click', function(e) {
        this.href = externalTargetUrl;
        e.stopPropagation();
      });
    }
  };

  document.addEventListener('cdm-search-page:ready', function(event) {
    addExternal();
  });

  document.addEventListener('cdm-search-page:update', function(event) {
    addExternal();
  });

  document.addEventListener('cdm-collection-search-page:ready', function(event) {
    addExternal();
  });

  document.addEventListener('cdm-collection-search-page:update', function(event) {
    addExternal();
  });

})();