// https://help.oclc.org/Metadata_Services/CONTENTdm/Advanced_website_customization/Customization_cookbook/Domain_name-specific_redirect

// client-side redirects from one to our boutique URL
// yes, this should be done server-side from OCLC
// I added additional variable Searchquery 
// ref https://developer.mozilla.org/en-US/docs/Web/API/Location

(function() {
'use strict';

  function urlRedirector() {
    const targetPath = window.location.pathname;
    const searchQuery = window.location.search;
    const baseUrl = window.location.origin
      ? window.location.origin + '/'
      : window.location.protocol + '//' + window.location.host + '/';
    if (baseUrl == badUrl) {
      window.location = goodUrl + targetPath + searchQuery;
    }
  }

  const badUrl = 'https://cdm16014.contentdm.oclc.org/';
  const goodUrl = 'https://cplorg.contentdm.oclc.org';

  document.addEventListener('cdm-app:ready', function() {
    // executes the first time a user arrives at the site
    urlRedirector();
  });

  ['cdm-home-page:enter', 'cdm-home-page:update',
  'cdm-search-page:enter','cdm-search-page:update',
  'cdm-collection-page:enter','cdm-collection-page:update',
  'cdm-custom-page:enter','cdm-custom-page:update',
  'cdm-item-page:enter','cdm-item-page:update',
  'cdm-about-page:enter','cdm-about-page:update'
  ].forEach(function(e) {
      document.addEventListener(e,urlRedirector);
    });

})();

/* version history

1.3 - 2020 March 1 - add listeners for many more page classes
1.2 - 2020 February 14 - added full path preservation in redirect target
1.1 - 2019 August 9 - variable-ized the redirect URLs
1.0 - 2018 June - initial implementation

*/